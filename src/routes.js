import home from "./components/home"
import page from "./components/page"

export const routes=[
    {path: '' , component: home, name: "home"},
    {path: '*', redirect: {name: "home"}},
    {path: '/:activePage', component: page, name: "page"},
];