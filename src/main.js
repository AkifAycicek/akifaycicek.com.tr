import Vue from 'vue'
import App from './App.vue'
import VueRouter from "vue-router"
import { routes } from "./routes"
import Vuelidate from "vuelidate"
import axios from "axios"

axios.defaults.baseURL="/php/"

Vue.config.productionTip = false

Vue.use(Vuelidate);

Vue.use(VueRouter);
const router = new VueRouter({
  routes,
  mode:"history",
});

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
