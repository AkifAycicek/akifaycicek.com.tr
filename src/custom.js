export function expandContent(content,delay,duration){
  content.style.overflow="hidden"
  content.style.height="0px"
  content.style.transition="height "+duration+"s cubic-bezier(0.77, 0, 0.175, 1)"
  setTimeout(() => {
      content.style.setProperty("height",content.scrollHeight+"px","important")
      setTimeout(() => {
        content.removeAttribute("style")
      }, duration*1000);
  }, delay);
}